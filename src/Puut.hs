module Puut where
import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Color

data Puu = 
    Puu
    {
        puu_korkeus :: Float,
        puu_latvanLeveys :: Float,
        puu_rungonLeveys :: Float,
        puu_sijainti :: Float
    }

laskeLatvanKeskiosanSäde,laskeLatvanSivuosanSäde :: Puu -> Float
laskeLatvanKeskiosanSäde puu = (puu_latvanLeveys puu) / 2
laskeLatvanSivuosanSäde puu = (puu_latvanLeveys puu) / 3

laskeLatvanSivuosanOffsetX, laskeLatvanSivuosanOffsetY :: Puu -> Float
laskeLatvanSivuosanOffsetX puu = (puu_latvanLeveys puu) / 4
laskeLatvanSivuosanOffsetY puu = (puu_latvanLeveys puu) / 4

piirraPuu :: Puu -> Picture
piirraPuu puu = 
    let
        latvanKeskiosanSäde = laskeLatvanKeskiosanSäde puu
        latvanSivuosanSäde = laskeLatvanSivuosanSäde puu
        
        latvanSivuosanOffsetX = laskeLatvanSivuosanOffsetX puu
        latvanSivuosanOffsetY = laskeLatvanSivuosanOffsetY puu

        runkoLatvaOffset = ((puu_korkeus puu)/2) + latvanKeskiosanSäde

        latvaKeski = translate 0 runkoLatvaOffset (color green (circleSolid latvanKeskiosanSäde))
        latvaVasen = translate (-latvanSivuosanOffsetX) (-latvanSivuosanOffsetY+runkoLatvaOffset) 
                                (color green (circleSolid latvanSivuosanSäde))
        latvaOikea = translate (latvanSivuosanOffsetX) (-latvanSivuosanOffsetY+runkoLatvaOffset) 
                                (color green (circleSolid latvanSivuosanSäde))

        runko = color orange (rectangleSolid (puu_rungonLeveys puu) (puu_korkeus puu))
        puunKuva = runko <> latvaKeski <> latvaVasen <> latvaOikea
        paikoillaan = translate (puu_sijainti puu) (puu_korkeus puu / 2) puunKuva
    in paikoillaan

rungonNurkkaPisteet :: Puu -> (Point, Point)
rungonNurkkaPisteet puu = 
    let
        vasenAla = (puu_sijainti puu - (puu_rungonLeveys puu / 2), 0)
        oikeaYlä = (puu_sijainti puu + (puu_rungonLeveys puu / 2), puu_korkeus puu)
    in (vasenAla, oikeaYlä)


latvanHitboxSegmentit :: Puu -> [(Point,Point)]
latvanHitboxSegmentit puu =
    let
        x = puu_sijainti puu
        korkeus = puu_korkeus puu
        latvanKeskiosanSäde = laskeLatvanKeskiosanSäde puu
        latvanSivuosanSäde = laskeLatvanSivuosanSäde puu
        latvanSivuosanOffsetX = laskeLatvanSivuosanOffsetX puu
        latvanSivuosanOffsetY = laskeLatvanSivuosanOffsetY puu
        
        (keskiosanOrigoX,keskiosanOrigoY) = (x, korkeus + latvanKeskiosanSäde)
        (vasenOrigoX,vasenOrigoY) = (keskiosanOrigoX - latvanSivuosanOffsetX, keskiosanOrigoY - latvanSivuosanOffsetY)
        (oikeaOrigoX,oikeaOrigoY) = (keskiosanOrigoX + latvanSivuosanOffsetX, keskiosanOrigoY - latvanSivuosanOffsetY)

        -- keskiosan ylin piste
        piste1 = (keskiosanOrigoX, keskiosanOrigoY + latvanKeskiosanSäde)
        -- keskiosan vasen piste
        piste2 = (keskiosanOrigoX - (latvanKeskiosanSäde / (sqrt 2)), 
                  keskiosanOrigoY + (latvanKeskiosanSäde / (sqrt 2)))
        -- vasemman osan vasen piste
        piste3 = (vasenOrigoX - latvanSivuosanSäde, vasenOrigoY)
        -- vasemman osan alin piste
        piste4 = (vasenOrigoX,vasenOrigoY - latvanSivuosanSäde)
        -- oikean osan alin piste
        piste5 = (oikeaOrigoX,oikeaOrigoY - latvanSivuosanSäde)
        -- oikean osan oikea piste
        piste6 = (oikeaOrigoX + latvanSivuosanSäde, oikeaOrigoY)
        -- keskiosan oikea piste
        piste7 = (keskiosanOrigoX + (latvanKeskiosanSäde / (sqrt 2)), 
                  keskiosanOrigoY + (latvanKeskiosanSäde / (sqrt 2)))

        segmentti1 = (piste1, piste2)
        segmentti2 = (piste2, piste3)
        segmentti3 = (piste3, piste4)
        segmentti4 = (piste4, piste5)
        segmentti5 = (piste5, piste6)
        segmentti6 = (piste6, piste7)
        segmentti7 = (piste7, piste1)

    in [segmentti1, segmentti2, segmentti3, segmentti4, segmentti5, segmentti6, segmentti7]