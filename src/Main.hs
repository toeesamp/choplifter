module Main where
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
-- import Graphics.Gloss.Geometry.Angle
import Graphics.Gloss.Data.Vector
import Graphics.Gloss.Geometry.Line
import Prelude hiding (Down)
import Data.List (partition)
import Aritmetiikka
import Talot
import Hemmot
import Kopteri
import Puut

alkutilanne :: PeliTilanne
alkutilanne =
    (GameOn(Peli 
            0           -- aika
            (luoKopteri (0,0))
            -- [Talo 800 500 700,
            --  Talo 800 500 (-700),
            --  Talo 300 500 1300]
            [Talo 800 500 700]
            [Hemmo (700,800), 
             Hemmo (900, 800)]
            [Puu 400 200 50 (-700),
            Puu 700 300 100 (1200)
            ]
            ))



main :: IO ()
main = play 
        (InWindow "Choplifter" (800,800) (2700,700))
        (light blue)
        144
        alkutilanne
        piirräPeliTilanne
        reagoiPeliTilanne
        päivitäPeliTilanne

reagoiPeliTilanne :: Event -> PeliTilanne -> PeliTilanne
reagoiPeliTilanne tapahtuma pelitilanne =
    case pelitilanne of
        GameOver peli -> GameOver peli
        GameOn peli -> GameOn (reagoi tapahtuma peli)

reagoi :: Event -> Choplifter -> Choplifter
reagoi tapahtuma peli = 
    case tapahtuma of
        EventKey (Char 'w') Down _ _ -> kopterille (muutaTehoa  (2)) peli 
        EventKey (Char 's') Down _ _ -> kopterille (muutaTehoa (-2)) peli 
        EventKey (Char 'a') Down _ _ -> kopterille (kallista   (-8)) peli 
        EventKey (Char 'd') Down _ _ -> kopterille (kallista    (8)) peli 
        _ -> peli


päivitäPeliTilanne :: Float -> PeliTilanne -> PeliTilanne
päivitäPeliTilanne aikaEdellisestä pelitilanne =
    case pelitilanne of 
        GameOver peli -> GameOver peli
        GameOn peli -> case (törmääköTaloon (kopteriTörmäysviivat (cl_kopteri peli)) (cl_talot peli),
                             törmääköPuuhun (kopteriTörmäysviivat (cl_kopteri peli)) (cl_puut peli)) of
                        (Nothing, Nothing) -> GameOn (päivitäPeliä aikaEdellisestä peli)
                        (_, Just Roottori) -> GameOver peli
                        (_, Just Laskuteline) -> GameOver peli
                        (Just Roottori,_) -> GameOver peli
                        (Just Laskuteline,_)
                            | onkoHyväLaskeutuminen (cl_kopteri peli)
                                -> GameOn (päivitäPeliä aikaEdellisestä (kopterille laskeudu peli))
                            | otherwise -> GameOver peli

kopterille :: (Kopteri -> Kopteri) -> Choplifter -> Choplifter
kopterille f peli = peli{cl_kopteri = f (cl_kopteri peli)}


päivitäPeliä :: Float -> Choplifter -> Choplifter
päivitäPeliä  aikaEdellisestä edellinenTila = 
    case edellinenTila of
        Peli aika kopteri talot hemmot puut
            ->  let
                    paikka = kop_paikka kopteri
                    nouseekoKyytiin hemmo = magV (hemmo_sijainti hemmo #- (kop_paikka kopteri)) < 50
                    (hemmotKopteriin, hemmotUlkona) = partition nouseekoKyytiin hemmot
                in Peli (aika + aikaEdellisestä)
                        (päivitäKopteria aikaEdellisestä (genericLength hemmotKopteriin) kopteri)
                        talot
                        (map (päivitäHemmoa (flip korkeusKohdassa edellinenTila) 
                                            paikka)
                              hemmotUlkona)
                        puut


törmääköTaloon :: ((Point,Point),(Point,Point)) -> [Talo] -> Maybe TörmäysKohta
törmääköTaloon kopterinTörmäysviivat talot = fmap maximum1 (nonEmpty (mapMaybe törmääköYhteenTaloon talot))
    where
        törmääköYhteenTaloon talo = 
            let
                ((ala1,ala2), (ylä1,ylä2)) = kopterinTörmäysviivat
                (va,oy)    = nurkkaPisteet talo
            in case (not (segClearsBox ala1 ala2 va oy), not (segClearsBox ylä1 ylä2 va oy)) of
                (True, False) -> Just Laskuteline
                (False, False) -> Nothing
                _ -> Just Roottori

törmääköPuuhun :: ((Point,Point),(Point,Point)) -> [Puu] -> Maybe TörmäysKohta
törmääköPuuhun kopterinTörmäysviivat puut = fmap maximum1 (nonEmpty (mapMaybe törmääköYhteenPuuhun puut))
    where
        ((ala1,ala2), (ylä1,ylä2)) = kopterinTörmäysviivat
        törmääköYhteenPuuhun puu =
            let 
                (runkoVasenAla, runkoOikeaYlä) = rungonNurkkaPisteet puu
                latvanSegmentit = latvanHitboxSegmentit puu
                osuukoRunkoon = (not (segClearsBox ala1 ala2 runkoVasenAla runkoOikeaYlä) 
                                || not (segClearsBox ylä1 ylä2 runkoVasenAla runkoOikeaYlä))
                osuukoLatvaan = elem True (map osuukoLatvanOsaan latvanSegmentit)
            in case osuukoRunkoon || osuukoLatvaan of
                False -> Nothing
                True -> Just Roottori

        osuukoLatvanOsaan (piste1,piste2) =
            let
                osuukoAlaviivaan = intersectSegSeg piste1 piste2 ala1 ala2
                osuukoYläviivaan = intersectSegSeg piste1 piste2 ylä1 ylä2
            in case (osuukoAlaviivaan,osuukoYläviivaan) of
                (Nothing,Nothing) -> False
                _ -> True


piirräPeliTilanne :: PeliTilanne -> Picture
piirräPeliTilanne pelitilanne = 
    case pelitilanne of
        GameOver peli -> piirräPeli peli <> translate (-400) 0 (color yellow (text "GAME OVER"))
        GameOn peli -> piirräPeli peli

piirräPeli :: Choplifter -> Picture
piirräPeli peli = 
    let
        aika = cl_aika peli
        talot = cl_talot peli
        puut = cl_puut peli

        ((va, oa),(vy, oy)) = kopteriTörmäysviivat (cl_kopteri peli)
        apuviivaAla = color red (line [va, oa])
        apuviivaYlä = color red (line [vy, oy])

        kopteriKuva = piirräKopteri aika (cl_kopteri peli)
        hemmoKuvat = map (piirräHemmo aika) (cl_hemmot peli)
        taloKuvat = map piirraTalo talot
        puuKuvat = map piirraPuu puut

        -- debugViesti = scale 0.5 0.5 (text (show (ceiling (magV (cl_nopeus peli))::Int) <> "   " <> show (ceiling kulma::Int)))

        peliKuva = kopteriKuva
                    <> maa
                    <> pictures taloKuvat
                    <> pictures hemmoKuvat
                    <> pictures puuKuvat
                    <> apuviivaAla
                    <> apuviivaYlä
                    -- <> debugViesti
    in scale 0.25 0.25 (translate 0 (-180) peliKuva)

piirräApuviiva :: (Point, Point) -> Picture
piirräApuviiva (piste1, piste2) = color red (line [piste1, piste2])

data PeliTilanne = GameOver Choplifter | GameOn Choplifter

data Choplifter = 
    Peli
    {
        cl_aika   :: Float,            -- ^ aika pelin alusta
        cl_kopteri :: Kopteri,
        cl_talot :: [Talo],
        cl_hemmot :: [Hemmo],
        cl_puut :: [Puu]
    }

korkeusKohdassa :: Float -> Choplifter -> Float
korkeusKohdassa kohta peli = maybe 0 maximum1 . nonEmpty . map (osuukoTaloon kohta) . cl_talot $ peli


maa :: Picture
maa = color green (translate 0 (-500) (rectangleSolid 5000 1000))
